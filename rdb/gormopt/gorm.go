package gormopt

import (
	"gorm.io/gorm"
)

type Option func(db *gorm.DB) *gorm.DB

func Select(query interface{}, args ...interface{}) Option {
	return func(db *gorm.DB) *gorm.DB { return db.Select(query, args...) }
}

func Omit(columns ...string) Option {
	return func(db *gorm.DB) *gorm.DB {
		return db.Omit(columns...)
	}
}

func Where(query interface{}, args ...interface{}) Option {
	return func(db *gorm.DB) *gorm.DB { return db.Where(query, args...) }
}

func Order(value interface{}) Option {
	return func(db *gorm.DB) *gorm.DB { return db.Order(value) }
}

func Limit(limit int) Option {
	return func(db *gorm.DB) *gorm.DB { return db.Limit(limit) }
}

func Offset(offset int) Option {
	return func(db *gorm.DB) *gorm.DB { return db.Offset(offset) }
}

func Scopes(funcs ...func(*gorm.DB) *gorm.DB) Option {
	return func(db *gorm.DB) *gorm.DB { return db.Scopes(funcs...) }
}

func Apply(db *gorm.DB, opts ...Option) *gorm.DB {
	for _, opt := range opts {
		db = opt(db)
	}
	return db
}
