package rdb

import (
	"strings"

	"gorm.io/driver/mysql"
	"gorm.io/gorm"
	"gorm.io/gorm/logger"
)

type Config struct {
	Name     string
	Driver   string
	DSN      string
	LogLevel logger.LogLevel
	Default  bool
}

var (
	DBs       = map[string]*gorm.DB{}
	defaultDB *gorm.DB
)

func Init(configs []Config) {
	for _, config := range configs {
		var db *gorm.DB
		var err error

		switch strings.ToLower(config.Driver) {
		case `mysql`:
			db, err = gorm.Open(mysql.Open(config.DSN), &gorm.Config{})
			if err != nil {
				panic(err)
			}

			db.Logger.LogMode(config.LogLevel)

			DBs[config.Name] = db
		}

		if config.Default || len(configs) == 1 {
			defaultDB = db
		}
	}
}

func DefaultDB() *gorm.DB {
	return defaultDB
}
