package config

import (
	"gitee.com/xiaofangjian/dadi/rdb"
	"github.com/BurntSushi/toml"
)

type ConfigFile struct {
	App      App
	Database []rdb.Config
	// Redis    []redis.RedisConfig
}

type App struct {
	Secret string
}

var appConfig ConfigFile
var appConfigMap map[string]interface{}

func InitContent(content []byte) (err error) {
	_, err = toml.Decode(string(content), &appConfig)
	if err != nil {
		return
	}

	_, err = toml.Decode(string(content), &appConfigMap)
	if err != nil {
		return
	}

	return
}

func Config() ConfigFile {
	return appConfig
}

func ConfigMap() map[string]interface{} {
	return appConfigMap
}
